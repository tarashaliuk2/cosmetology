<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class Photo extends Model
{
    public $timestamps = false;

    public static function getPhotosList(){
        return DB::table('photos')->get();
    }

    public static function setPhotos($files, $photo_category){
        foreach($files as $file) {

            //prepare destination path and generate name
            $destinationPath = public_path().'/photos/shares/gallery/';
            echo '1';
            $filename = str_slug($file->getClientOriginalName()).'.'.$file->getClientOriginalExtension();
            echo  '2';
            $filename = time().$filename;
            echo '3';
            $file->move($destinationPath, $filename);
            echo '4';
            $photo = new Photo();
            $photo->path = '/photos/shares/gallery/'.$filename;
            $photo->category_id = $photo_category->id;
            $photo->save();

            self::setPhotoSize($filename, $destinationPath, 800, 600);
        }
    }

    public static function setPhotoSize($filename, $path, $width, $height){


        $img = Image::make(asset('/photos/shares/gallery/'.$filename));
        echo '5';
        $img->fit($width, $height);
        echo '6';
        $img->save($path.$filename);
        echo '7';
    }
}
