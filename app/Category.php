<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    public $timestamps = false;

    protected $table = 'categories';

    public function procedures()
    {
        return $this->hasMany(Procedures::class, 'category_id');
    }

    public static function getCategoriesList(){
        return DB::table('categories')->get();
    }
}
