<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PhotoCategory extends Model
{
    protected $fillable = ['name'];
    public $timestamps = false;

    protected $table = 'photo_categories';

    public function photos(){
        return $this->hasMany(Photo::class, 'category_id');
    }

    public static function getCategoriesList(){
        return DB::table('photo_categories')->get();
    }

    public static function setCategory($data){
        $photo_category = new PhotoCategory();
        $photo_category->fill($data);
        $photo_category->save();

        return $photo_category;
    }
}
