<?php

namespace App;

use App\Parts_of_body;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Procedures extends Model
{

    protected $fillable = ['name', 'text', 'img', 'slug', 'img', 'active', 'category_id'];
    public $timestamps = false;

    public function parts_of_body(){
        return $this->belongsToMany(Parts_of_body::class, 'procedures_parts_price', 'id_procedure', 'id_part_of_body')
            ->withPivot('price');;
    }



    public static function getProceduresList(){
        return DB::table('procedures')->get();
    }

    public static function getProcedure($slug){
        return Procedures::getProceduresList()->where('slug', $slug)->first();
    }

    public static function getProceduresListActive(){
        return DB::table('procedures')->where('active', 1)->get();
    }

    public static function getProcedureActive($slug){
        return Procedures::getProceduresListActive()->where('slug', $slug)->first();
    }
}
