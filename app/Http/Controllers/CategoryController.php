<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function index(){
        $categories = Category::getCategoriesList();

        return view('admin.categories.categories-list')->with(['categories' => $categories,
                                                                    'newInput' => 'hidden']);
    }

    public function indexWithNewInput(){
        $categories = Category::getCategoriesList();

        return view('admin.categories.categories-list')->with(['categories' => $categories,
            'newInput' => 'text']);
    }

    public function create(){
        return view('admin.categories.create-category');
    }

    public function store(Request $request){
        $data = $request->all();
        dd($data);
        $category = new Category();
        $category->fill($data)->save();

        return redirect(route('categoriesList'));
    }

    public function edit($id){
        $category = Category::find($id);

        return view('admin.categories.categories')->withCategory($category);
    }

    public function update(Request $request){

        $data = $request->all();
//        dd($data);
        $keys = array_keys($data);  //get keys of data array
        array_shift($keys);         //delete [0] element of array (token)
//        dd($keys);
        foreach ($keys as $key){
            $category = Category::find($key);
            $category->category_name = $data[$key];
            $category->update();
        }

        return redirect(route('categoriesList'));
    }

    public function delete($id){
        $category = Category::find($id);

        $category->delete();

        return redirect(route('categoriesList'));
    }
}
