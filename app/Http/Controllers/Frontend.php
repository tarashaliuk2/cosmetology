<?php

namespace App\Http\Controllers;

use App\Procedures;
use Illuminate\Http\Request;

class Frontend extends Controller
{
    public function showMainPage()
    {
        $procedures = Procedures::getProceduresListActive();

        return view('index')->with(['procedures' => $procedures,
        ]);
    }

}
