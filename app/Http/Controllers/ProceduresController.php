<?php

namespace App\Http\Controllers;

use App\Category;
use App\Procedures;
use App\Parts_of_body;
use Illuminate\Http\Request;
use http\Url;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
//use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;

class ProceduresController extends Controller
{
    public function index()
    {
        $procedures = Procedures::getProceduresList();

        return view('admin.procedures.procedures-list')->with(['procedures' => $procedures]);
    }

    public function create()
    {
        $categories = Category::getCategoriesList();
//        dd($categories);
        return view('admin.procedures.create-procedure')->withCategories($categories);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $data['slug'] = str_slug($request->name);

        if ($data['img'] != null){
            $img = Image::make(asset($data['img']));
            $img->fit(350, 170);
            $img->save(public_path($data['img']));
        }

        dd($data);
        $procedure = new Procedures();
        $procedure->fill($data);
        $procedure->save();

        $procedure->parts_of_body()->attach([1 => ['price' => $request->price1]]);
        $procedure->parts_of_body()->attach([2 => ['price' => $request->price2]]);
        $procedure->parts_of_body()->attach([3 => ['price' => $request->price3]]);
        $procedure->parts_of_body()->attach([4 => ['price' => $request->priceMain]]);

        return redirect(route('proceduresList'));
    }

    public function show($slug)
    {
        $procedures = Procedures::getProceduresListActive();
        $procedure = Procedures::getProcedureActive($slug);

        return view('procedure')->with(['procedure' => $procedure,
            'procedures' => $procedures]);
    }


    public function edit($slug)
    {
        $procedure = Procedures::where('slug', $slug)->first();
        $categories = Category::getCategoriesList();

        return view('admin.procedures.procedure')->with(['procedure' => $procedure,
                                                    'categories' => $categories]);
    }


    public function update(Request $request, $slug)
    {
        $procedure = Procedures::where('slug', $slug)->first();

        $data = $request->all();
        $data['slug'] = str_slug($request->name);

        if ($data['img'] != null){
            $img = Image::make(asset($data['img']));
            $img->fit(350, 170);
            $img->save(public_path($data['img']));
        }

        $procedure->update($data);

        $procedure->parts_of_body()->syncWithoutDetaching([1 => ['price' => $request->price1]]);
        $procedure->parts_of_body()->syncWithoutDetaching([2 => ['price' => $request->price2]]);
        $procedure->parts_of_body()->syncWithoutDetaching([3 => ['price' => $request->price3]]);
        $procedure->parts_of_body()->syncWithoutDetaching([4 => ['price' => $request->priceMain]]);

        return redirect(route('proceduresList'));
    }

    public function confirmDeleting($slug){
        $procedure = Procedures::where('slug', $slug)->first();

        return view('admin.procedures.confirm-deleting')->withProcedure($procedure);
    }


    public function destroy($slug)
    {
        $procedure = Procedures::where('slug', $slug)->first();

        $procedure->parts_of_body()->detach();
        $procedure->delete();

        return redirect(route('proceduresList'));
    }


    public function hide($slug){
        $procedure = Procedures::where('slug', $slug)->first();
        $procedure->update(['active' => 0]);

        return redirect(route('proceduresList'));
    }

    public function activate($slug){
        $procedure = Procedures::where('slug', $slug)->first();
        $procedure->update(['active' => 1]);

        return redirect(route('proceduresList'));
    }

    public function showPrice(){
        $proceduresPrice = DB::table('procedures')
            ->join('procedures_parts_price',    'procedures.id',            '=', 'procedures_parts_price.id_procedure')
            ->join('categories',                'procedures.category_id',   '=', 'categories.id')
            ->where('procedures.active', 1)
            ->select('name', 'price', 'id_procedure', 'id_part_of_body', 'category_name', 'slug')
            ->orderBy('procedures_parts_price.id', 'asc')
            ->get();

        $categories = Category::getCategoriesList();

        $procedures = Procedures::getProceduresListActive();

        return view('price')->with(['proceduresPrice' =>$proceduresPrice,
                                            'categories' => $categories,
                                            'procedures' => $procedures]);
    }

}
