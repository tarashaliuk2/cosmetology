<?php

namespace App\Http\Controllers;

use App\Photo;
use App\PhotoCategory;
use App\Procedures;

use Illuminate\Http\Request;

class GalleryController extends Controller
{
    public function show()
    {
        $procedures = Procedures::getProceduresListActive();
        $categories = PhotoCategory::getCategoriesList();
        $photos = Photo::getPhotosList();

        return view('gallery')->with(['procedures' => $procedures,
                                            'categories' => $categories,
                                            'photos' => $photos]);
    }



    public function index()
    {
        $categories = PhotoCategory::getCategoriesList();
        $photos = Photo::getPhotosList();

        return view('admin.gallery.gallery')->with(['categories' => $categories,
                                                    'photos' => $photos]);
    }



    public function createCategory()
    {
        return view('admin.gallery.photo-category');
    }



    public function storeCategory(Request $request)
    {
        $data = $request->all();

        $files = $request->file('img');
        if ($files[0] != '')
            Photo::setPhotos($files, PhotoCategory::setCategory($data));

        return redirect(route('showGalleryAdmin'));
    }



    public function edit($id)
    {

        $category = PhotoCategory::find($id);

        return view('admin.gallery.edit-photo-category')->withCategory($category);
    }



    public function update(Request $request, $id)
    {
        $data = $request->all();

        $category = PhotoCategory::find($id);
        $category->update($data);

        $files = $request->file('img');

        if ($files[0] != '')
            Photo::setPhotos($files, $category);


        return redirect(route('showGalleryAdmin'));
    }



    public function deletePhoto($id)
    {
        $photo = Photo::find($id);
        unlink(public_path().$photo->path);
        $photo->delete();

        return redirect(route('showGalleryAdmin'));
    }



    public function confirmDeleting($id)
    {
        $category = PhotoCategory::find($id);

        return view('admin.gallery.confirm-deleting')->withCategory($category);
    }



    public function deleteCategory($id)
    {
        $category = PhotoCategory::find($id);

        $files = Photo::where('category_id', '=', $id)->get();

        foreach ($files as $file){
            $filePath = public_path().$file->path;
            unlink($filePath);
            $file->delete();
        }

        $category->delete();

        return redirect(route('showGalleryAdmin'));
    }


}
