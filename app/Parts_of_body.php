<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parts_of_body extends Model
{

    public $timestamps = false;

    protected $table = 'parts_of_body';
//    public function procedures(){
//        return $this->belongsToMany('Procedures', 'procedures_part_price', 'id_part_of_body', 'id_procedure')
//            ->withPivot('price');
//    }
}
