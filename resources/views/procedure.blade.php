@extends('layouts.layout')

@section('title', $procedure->name)


@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <div class="post-content">
                    <h3>{{$procedure->name}}</h3>
                    {!! $procedure->text !!}
                </div>
            </div>
            
            <div class="col-sm-3 hidden-xs">
                <div class="post-sidebar">
                    @foreach($procedures as $proceduresItem)
                        <a @if(route('showProcedure', $proceduresItem->slug) == Request::url()) class="active" @endif href="{{route('showProcedure', $proceduresItem->slug)}}">
                            {{$proceduresItem->name}}
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection