@extends('layouts.layout')

@section('title', 'Beauty Doctor - косметологія')

@section('content')
    <div class="container">
        <div class="row">
            <h3>Прайс-лист послуг</h3>
            <hr class="rose-line-small"><br>

            @foreach($categories as $category)
                <div class="col-sm-6">
                    <table class="price-table">
                        <tbody>
                            <tr>
                                <td width="80%">
                                    <h4>
                                        {{$category->category_name}}
                                    </h4>
                                </td>
                                <td>&nbsp;</td>
                            </tr>

                                @foreach($proceduresPrice as $procedure)
                                    @if($procedure->category_name == $category->category_name)
                                        @if($procedure->id_part_of_body == 4 && $procedure->price != null)
                                            <tr>
                                                <td><b>{{$procedure->name}}</b></td>
                                                <td>{{$procedure->price}}</td>
                                            </tr>
                                        @else

                                            @if($procedure->id_part_of_body == 1 && $procedure->price != null)
                                                <tr>
                                                    <td><b>{{$procedure->name}}</b> обличча</td>
                                                    <td>{{$procedure->price}}</td>
                                                </tr>
                                            @elseif($procedure->id_part_of_body == 2 && $procedure->price != null)
                                                <tr>
                                                    <td><b>{{$procedure->name}}</b> обличча, шия</td>
                                                    <td>{{$procedure->price}}</td>
                                                </tr>
                                            @elseif($procedure->id_part_of_body == 3 && $procedure->price != null)
                                                <tr>
                                                    <td><b>{{$procedure->name}}</b> обличча, шия, деколтьте</td>
                                                    <td>{{$procedure->price}}</td>
                                                </tr>
                                            @endif
                                        @endif
                                    @endif
                                @endforeach

                        </tbody>
                    </table>
                </div>
            @endforeach
        </div>
    </div>

@endsection
