@extends('layouts.layout')

@section('title', 'Beauty Doctor - косметологічний кабінет')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div id="carousel" class="carousel fade width100proc" data-ride="carousel">
                <!-- Индикаторы слайдов -->
                <ol class="carousel-indicators hidden-xs">
                    <li data-target="#carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel" data-slide-to="1"></li>
                    <li data-target="#carousel" data-slide-to="2"></li>
                </ol>
                <!-- Конец индикаторов слайдов -->
                <!-- Slidy -->
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="{{asset('img/main-page/slider1.jpg')}}" alt="First slide">
                    </div>
                    <div class="item">
                        <img src="{{asset('img/main-page/slider2.jpg')}}" alt="Second slide">
                    </div>
                    <div class="item">
                        <img src="{{asset('img/main-page/slider3.jpg')}}" alt="Second slide">
                    </div>


                </div>
                <!-- Koniec slidów -->

                <!-- Przyciski kierowanie sliderem -->
                <a href="#carousel" class="carousel-control left" href="#carousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a href="#carousel" class="carousel-control right" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                <!-- Koniec przycisków kierowanie sliderem -->
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <h2 class="text-center">Gabinet kosmetologiczny eBeautyLab</h2>
            <hr class="small-rose-hline">
            <h4 class="text-center">Kadr - to inowacyjne studio fotograficzne, z niezwykle bogatym wyposażeniem i naprawde oryginalnym wnętrzem</h4>
            <h4 class="text-center">340<sup>2</sup> pomogą Ci w realizacji wszystkich twórczych pomysłów</h4>
        </div>
    </div>
    <div class="container">
        <div class="row procedures">

            @foreach($procedures as $procedure)
            <div class="col-sm-4 col-xs-6">
                <a href="{{route('showProcedure', $procedure->slug)}}">
                    <img src="{{asset($procedure->img)}}">
                    <p>{{$procedure->name}}</p>
                </a>
            </div>
            @endforeach


        </div>
    </div>

    @endsection
