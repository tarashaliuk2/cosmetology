@extends('layouts.layout')

@section('title', 'Галерея')


@section('content')
<div class="container">

    @foreach($categories as $category)
        <div class="row">
            <div class="col-xs-12">
                <h3>{{$category->name}}</h3>
                <hr class="rose-line-small">
            </div>
        </div>


        <div class="row photos">
            @foreach($photos as $photo)
                @if($photo->category_id == $category->id)
                <div class="col-sm-3">
                    <a href="{{asset($photo->path)}}" data-rel="lightcase:{{$category->name}}">
                        <img class="img-responsive" src="{{asset($photo->path)}}">
                    </a>
                </div>
                @endif
            @endforeach
        </div>
    @endforeach
</div>

@endsection

@section('js')
    <!--  Gallery  -->
    {{--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>--}}
    <script type="text/javascript" src="{{asset('js/lightcase.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('a[data-rel^=lightcase]').lightcase();
        });
    </script>
@endsection


