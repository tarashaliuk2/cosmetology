<!DOCTYPE html>
<html lang="ua-UA">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0;" />

    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/styles.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/lightcase.css')}}">
</head>

<body>

<header>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-5 col-xs-12">
                <a href="{{route('showMainPage')}}">
                    <img src="{{asset('img/logo.png')}}" class="vcenter img-responsive logo" alt="logo">
                </a>
            </div>

            <div class="col-md-8">
                <div class="row">
                    <a href="{{route('showReservationPage')}}" class="btn-order-header hidden-xs">Замовити візит</a>
                </div>

                <!-- Początek navbara -->
                <div class="row">
                    <div class="navbar navbar-default">
                        <div id="nav-container" class="containers-fluid container-menu">
                            <div class="navbar-header">
                                <button class="btn-order-header hidden-sm hidden-md hidden-lg">
                                    Замовити візит
                                </button>
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#resp-menu">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="collapse navbar-collapse width100proc" id="resp-menu">
                                <ul class="nav navbar-nav">
                                    <li><a href="{{route('showMainPage')}}">Головна</a></li>

                                    <li class="dropdown">
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Послуги
                                            <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu" rule="menu">
                                            @foreach($procedures as $procedure)
                                                <li>
                                                    <a href="{{route('showProcedure',$procedure->slug)}}">
                                                        {{$procedure->name}}
                                                    </a>
                                                </li>
                                            @endforeach

                                        </ul>
                                    </li>
                                    <li><a href="{{route('showPrice')}}">Прайс-лист</a></li>
                                    <li><a href="{{route('showSales')}}">Акції</a></li>
                                    <li><a href="{{route('showGallery')}}">Галерея</a></li>
                                    <li><a href="{{route('showContacts')}}">Контакти</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>


@yield('content')


<div class="container-fluid paralax">
    <div class="row">
        <div class="col-sm-8">
            <h3>Запишись на візит online</h3>
        </div>
        <div class="col-sm-4">
            <button class="center-block">
                Записатися
            </button>
        </div>

    </div>
</div>


<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <h4>Адрес</h4>
                <p>
                    Івано-Франківськ <br> пл. Шептицького 12
                </p>
            </div>
            <div class="col-sm-3 ">
                <h4>Графік роботи</h4>
                <p>
                    пн-пт 9.00-16.00 <br> сб-нд 11.00-15.00
                </p>
            </div>

            <div class="col-sm-3 ">
                <h4>Контакти</h4>
                <p>
                    <b>тел.:</b> 0974512841, 0985489487 <br>
                    <b>email:</b> info@ebeautylab.ua
                </p>
            </div>
            <div class="col-sm-3 socnets">
                <h4>Соцмережі</h4>
                <a href="#">
                    <img src="{{asset('img/icons/facebook.png')}}" class="img-responsive" alt="">
                </a>
                <a href="#">
                    <img src="{{asset('img/icons/instagram.png')}}" class="img-responsive" alt="">
                </a>
                <a href="#">
                    <img src="{{asset('img/icons/vk.png')}}" class="img-responsive" alt="">
                </a>
            </div>

        </div>
        <div class="row copyrights">
            <p class="text-center"> &copy; 2017 <strong>eBeautyLab</strong> <br> Design and develop by <a href="taras-haliuk.com">Taras Haliuk</a></p>
        </div>
    </div>
</footer>









<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{asset('js/bootstrap.js')}}"></script>

<script type="text/javascript" src="{{asset('js/lightcase.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('a[data-rel^=lightcase]').lightcase();
    });
</script>

@yield('js')


</body>

</html>