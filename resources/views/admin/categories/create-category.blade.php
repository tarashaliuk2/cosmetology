@extends('layouts.app')

@section('title', 'Нова категорія')

@section('content')
    <div class="container">
        <div class="row">
            <h3>Нова категорія</h3>
        </div>
        <div class="row">
            <form action="{{route('storeCategory')}}" method="post">
                {{ csrf_field() }}
                <label for="category_name">Назва</label><br>
                <input type="text" class="input-category-name" name="category_name">

                <button type="submit" class="btn btn-success btn-block">Зберегти</button>
            </form>
        </div>
    </div>
@endsection