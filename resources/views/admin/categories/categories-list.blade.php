@extends('layouts.app')

@section('title', 'Категорії процедур')

@section('content')

    <div class="container">

        <a href="{{route('createCategory')}}" class="btn btn-primary btn-add-procedure">Додати</a>

        <form action="{{route('updateCategoriesList')}}" method="post">
            {{ csrf_field() }}
            @foreach($categories as $category)
                <div class="row">
                    <div class="col-xs-9 col-sm-10">
                        <input class="input-category-name" id="{{$category->id}}" name="{{$category->id}}" type="text" value="{{$category->category_name}}">
                    </div>
                    <div class="col-xs-3 col-sm-2">
                        <a href="{{route('deleteCategory', $category->id)}}" class="btn btn-danger btn-block">Видалити</a>
                    </div>
                </div>
            @endforeach

            <button class="btn btn-success btn-block" type="submit">Зберегти</button>
        </form>
    </div>

@endsection

@section('js')
    <script>
        var items=1;
        function AddItem() {
            div=document.getElementById("items");
            button=document.getElementById("add");
            newitem+="<input type=\"text\" name=\"item" + items;
            newitem+="\" size=\"45\"><br>";
            newnode=document.createElement("span");
            newnode.innerHTML=newitem;
            div.insertBefore(newnode,button);
        }
    </script>
@endsection