@extends('layouts.app')

@section('title', 'Додати фотокатегорію')

@section('content')
    <div class="container">
        <div class="row">
            <h2>Нова категорія</h2>
        </div>
        <div class="row">
            <form name="category_info" action="{{route('storePhotoCategory')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Назва категорії</label>
                    <input type="text" id="name" name="name" class="form-control" >
                </div>
                <div class="form-group">
                    <label for="file">Зображення</label>
                    <input id="img" name="img[]" type="file" value="" multiple/>
                    <div id="preview"></div>
                </div>
                <button type="submit" class="btn btn-success btn-block">Додати</button>
            </form>
        </div>
    </div>

@endsection
