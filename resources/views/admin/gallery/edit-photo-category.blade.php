@extends('layouts.app')

@section('title', 'Додати фотокатегорію')

@section('content')
    <div class="container">
        <div class="row">
            <h2>Редагування категорії</h2>
        </div>
        <div class="row">
            <form name="category_info" action="{{route('updatePhotoCategory', $category->id)}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Назва категорії</label>
                    <input type="text" id="name" name="name" class="form-control" value="{{$category->name}}">
                </div>
                <div class="form-group">
                    <label for="file">Додати зображення</label>
                    <input id="img" name="img[]" type="file" value="" multiple/>
                    <div id="preview"></div>
                </div>
                <button type="submit" class="btn btn-success btn-block">Зберегти</button>
            </form>
        </div>
    </div>

@endsection