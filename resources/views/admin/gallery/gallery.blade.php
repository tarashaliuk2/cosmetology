@extends('layouts.app')

@section('title', 'Управління галереєю')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <a href="{{route('createPhotoCategory')}}" class="btn btn-primary">Додати категорію</a>
            </div>
        </div>
        @foreach($categories as $category)
            <div class="div">
                <div class="row">
                    <div class="col-sm-9">
                        <h2 >{{$category->name}}</h2>
                    </div>
                    <div class="col-sm-3">
                        <a href="{{route('editPhotoCategory', $category->id)}}" class="btn btn-success btn-add-category">Редагувати</a>
                        <a href="{{route('confirmDeletingPhotoCategory', $category->id)}}" class="btn btn-danger btn-add-category">Видалити</a>
                    </div>
                </div>
                <div class="row photos">
                    @foreach($photos as $photo)
                        @if($photo->category_id == $category->id)
                            <div class="col-sm-2 col-xs-4">
                                <a href="{{asset($photo->path)}}" data-rel="lightcase:{{$photo->category_id}}">
                                    <img class="img-responsive" src="{{asset($photo->path)}}" >
                                </a>
                                <a href="{{route('deletePhoto', $photo->id)}}" class="btn btn-danger btn-delete">Видалити</a>
                            </div>
                        @endif
                    @endforeach
                </div>
                <hr class="hr-separator">
            </div>
        @endforeach
    </div>

@endsection

@section('js')
    <!--  Gallery  -->
    {{--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>--}}
    <script type="text/javascript" src="{{asset('js/lightcase.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('a[data-rel^=lightcase]').lightcase();
        });
    </script>
@endsection