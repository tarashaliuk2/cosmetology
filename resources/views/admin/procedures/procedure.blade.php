@extends('layouts.app')

@section('title', $procedure->name)

@section('head')
    <link rel="shortcut icon" type="image/png" href="{{ asset('vendor/laravel-filemanager/img/folder.png') }}">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <script type="text/javascript">
        <!--

        function validate_form ( )
        {
            valid = true;
            if (document.procedure_info.name.value == "" && document.procedure_info.text.value == ""
                && document.procedure_info.priceMain.value == "" && document.procedure_info.price1.value == "") {
                alert ( "Будь ласка, заповніть поля 'Заголовок', 'Наповнення' та 'Ціна'." );
                valid = false;
            }
            else if (document.procedure_info.name.value == "" && document.procedure_info.text.value == "") {
                alert ( "Будь ласка, заповніть поле 'Заголовок' та 'Наповнення'." );
                valid = false;
            }
            else if (document.procedure_info.name.value == ""){
                alert ( "Будь ласка, заповніть поле 'Заголовок'." );
                valid = false;
            }
            else if (document.procedure_info.text.value == ""){
                alert ( "Будь ласка, заповніть поле 'Наповнення'." );
                valid = false;
            } else if ( document.procedure_info.priceMain.value == "" && document.procedure_info.price1.value == "") {
                alert ( "Будь ласка, заповніть поле 'Ціна'." );
                valid = false;
            }
            else if (document.procedure_info.priceMain.value != "" &&(document.procedure_info.price1.value != ""
                    || document.procedure_info.price2.value != "" || document.procedure_info.price3.value != "")) {
                alert ( "Ціна повинна бути вказана з поділом на частини тіла АБО без поділу" );
                valid = false;
            }

            return valid;
        }

        //-->
    </script>
@endsection

@section('content')
    <div class="col-xs-12">

            <form name="procedure_info" action="{{route('procedureUpdate', $procedure->slug)}}" method="post" enctype="multipart/form-data" onsubmit="return validate_form ( )">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Назва</label>
                    <div class="row">
                        <div class="col-sm-10 col-xs-8">
                            <input type="text" class="form-control" id="name" name="name" value="{{$procedure->name}}">
                        </div>
                        <div class="col-sm-2 col-xs-4">
                            <a href="{{route('procedureConfirmDeleting', $procedure->slug)}}" class="btn btn-danger btn-block" style="float: right; margin-top: 0">Видалити</a>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="category">Категорія</label>
                    {{--<textarea class="form-control" id="category" name="category"></textarea>--}}
                    <select name="category_id" id="category_id">
                        @foreach($categories as $category)
                            <option @if ($procedure->category_id == $category->id) {{'selected'}} @endif value="{{$category->id}}">{{$category->category_name}}</option>
                        @endforeach

                    </select>
                </div>

                <div class="form-group">
                    <label for="text">Наповнення</label>
                    <textarea class="form-control" id="text" name="text">
                        {{$procedure->text}}
                    </textarea>
                </div>
                <div class="col-sm-4 form-group ">
                    <h4><b>Зображення</b></h4>
                    <div class="input-group">
                        <span class="input-group-btn">
                            <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                <i class="fa fa-picture-o"></i> Вибрати
                            </a>
                            <input id="thumbnail" class="form-control" type="hidden" name="img" value="{{$procedure->img}}">
                        </span>
                    </div>
                    <img src="{{$procedure->img}}" id="holder" style="margin-top:15px;max-height:100px;" alt="Нема зображення">
                </div>


                <div class="col-sm-4 form-group">
                    <h4><b>Ціна без поділу</b></h4>
                    <input type="text" id="priceMain" name="priceMain" value="{{$procedure->parts_of_body()->where('id_part_of_body', 4)->first()->pivot->price}}">
                </div>

                <div class="col-sm-4 form-group price">
                    <h4 class="text-center"><b>Ціна з поділом</b></h4>
                    <div class="col-sm-8">
                        <p for="price1">Лице</p>
                        <p for="price2">Лице, шия</p>
                        <p for="price3">Лице, шия, декольте</p>
                    </div>
                    <div class="col-sm-4 ">
                        <input type="text" id="price1" name="price1" value="{{$procedure->parts_of_body()->where('id_part_of_body', 1)->first()->pivot->price}}">
                        <input type="text" id="price2" name="price2"  value="{{$procedure->parts_of_body()->where('id_part_of_body', 2)->first()->pivot->price}}">
                        <input type="text" id="price3" name="price3"  value="{{$procedure->parts_of_body()->where('id_part_of_body', 3)->first()->pivot->price}}">
                    </div>
                </div>

                <button type="submit" class="btn btn-success btn-block">Опублікувати</button>

            </form>
        <br>






    </div>


@endsection

@section('js')


    {{--<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>--}}
    {{--<script src="{{asset('js/ass.js')}}"></script>--}}
    <script>
        var route_prefix = "{{ url(config('lfm.prefix')) }}";
    </script>

    <!-- CKEditor init -->
    <script src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>
    <script>
        $('textarea[name=text]').ckeditor({
            height: 700,
            filebrowserImageBrowseUrl: route_prefix + '?type=Images',
            filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
            filebrowserBrowseUrl: route_prefix + '?type=Files',
            filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}'
        });
    </script>


    <script>
        {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}
    </script>
    <script>
        $('#lfm').filemanager('image', {prefix: route_prefix});
    </script>





    <script>
        CKEDITOR.replace( 'text');
    </script>


@endsection