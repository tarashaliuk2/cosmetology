@extends('layouts.app')

@section('title', 'Видалення процедури')

@section('content')

    <div class="container">
        <div class="row">
            <h2 class="text-center">Ви дійно хочете видалити процедуру <br><b>{{$procedure->name}}</b>?</h2>
        </div>
        <div class="row">
            <div class="col-sm-4 col-xs-6">
                <a href="{{route('procedureDelete', $procedure->slug)}}" class="btn btn-success center-block">Taк</a>
            </div>
            <div class="col-sm-4 col-sm-offset-4 col-xs-6">
                <a href="{{route('showProcedureAdmin', $procedure->slug)}}" class="btn btn-danger  center-block">Ні</a>
            </div>

        </div>
    </div>

@endsection