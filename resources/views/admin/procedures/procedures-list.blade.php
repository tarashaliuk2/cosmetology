@extends('layouts.app')

@section('title', 'Список процедур')

@section('content')

    <div class="container">
        <a href="{{route('procedureCreate')}}" class="btn btn-success btn-add-procedure">Додати процедуру</a>


            @foreach($procedures as $procedure)
                <div class="row">
                    <div class="col-sm-2 col-xs-6">
                        <img class="img-list-procedures" src="{{$procedure->img}}" alt="No image">
                    </div>
                    <div class="col-sm-8 col-xs-6">
                        <a href="{{route('showProcedureAdmin', $procedure->slug)}}">
                            <h3>{{$procedure->name}}</h3>
                        </a>
                    </div>
                    <div class="col-sm-2">
                        @if($procedure->active == 1)
                            <a href="{{route('procedureHide', $procedure->slug)}}" class="btn btn-danger">Приховати</a>
                        @else
                            <a href="{{route('procedureActivate', $procedure->slug)}}" class="btn btn-success">Показати</a>
                        @endif
                    </div>

                </div>
                <hr>
            @endforeach


    </div>

@endsection