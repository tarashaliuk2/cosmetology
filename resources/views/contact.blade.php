@extends('layouts.layout')

@section('title', 'Контакти')

@section('content')
    <div class="container container-contacts">
        <div class="row ">
            <div class="col-sm-6 contacts">
                <h3>Контакти</h3>
                <hr class="rose-line-small"><br>


                <h4> <b>Адрес</b> </h4>
                <p>
                    Івано-Франківськ <br> пл. Шептицького 12

                </p>

                <h4 class="margin-top-20px"> <b>Інформація та запис на прийом</b> </h4>
                <p>
                    <b>тел.:</b> 0974512841, 0985489487 <br>
                    <b>email:</b> <a href="mailto:info@ebeautylab.ua">info@ebeautylab.ua</a> <br>
                    <b>Запис онлайн: </b><a href="#" class="online-reservation">тут</a>
                </p>


                <h4 class="margin-top-20px"> <b>Графік роботи</b> </h4>
                <p>
                    пн-пт 9.00-16.00 <br> сб-нд 11.00-15.00
                </p>
            </div>


            <div class="col-sm-6">
                <h3>Зворотній зв'язок</h3>
                <hr class="rose-line-small">
                <br>

                <form action="" method="post" name="contact-form">
                    <label for="name">
                        <p><b>Ім'я</b></p>
                        <input type="text" id="" name="name">
                    </label>

                    <div class="col-sm-6 email-input">
                        <label for="name">
                            <p><b>Email</b></p>
                            <input type="email" id="" name="email">
                        </label>
                    </div>

                    <div class="col-sm-6 tel-input">
                        <label for="name">
                            <p><b>Телефон</b></p>
                            <input type="text" id="" name="name">
                        </label>
                    </div>

                    <label for="message">
                        <p><b>Повідомлення</b></p>
                        <textarea rows="9" name="message"></textarea>
                    </label>

                    <input type="submit" name="submit" value="Відправити">


                </form>
            </div>
        </div>
    </div>
@endsection

