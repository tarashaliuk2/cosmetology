<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Frontend@showMainPage')->name('showMainPage');
Route::get('/page/{procedure}', 'ProceduresController@show')->name('showProcedure');
Route::get('price', 'ProceduresController@showPrice')->name('showPrice');
Route::get('gallery', 'GalleryController@show')->name('showGallery');
Route::get('contacts', 'Frontend@showContacts')->name('showContacts');
Route::get('sales', 'Frontend@showSales')->name('showSales');
Route::get('reserve-visit', 'ReservationController@show')->name('showReservationPage');



Route::group(['prefix' => 'admin/','middleware' => 'auth'], function (){


    Route::get('categories', 'CategoryController@index')->name('categoriesList');
    Route::post('categories/{id}/delete', 'CategoryController@delete')->name('deleteCategory');
    Route::post('categories/update', 'CategoryController@update')->name('updateCategoriesList');
    Route::get('categories/create', 'CategoryController@create')->name('createCategory');
    Route::post('categories/store', 'CategoryController@store')->name('storeCategory');
    Route::get('category/{id}', 'CategoryController@edit')->name('editCategory');

    Route::get('gallery', 'GalleryController@index')->name('showGalleryAdmin');                                     //(done)make frontend and implementation
    Route::get('gallery/category/create', 'GalleryController@createCategory')->name('createPhotoCategory');         //(done)make frontend and implementation
    Route::post('gallery/category/store', 'GalleryController@storeCategory')->name('storePhotoCategory');           //(done)make frontend and implementation
    Route::get('gallery/category/{category_id}/edit', 'GalleryController@edit')->name('editPhotoCategory');      //make frontend and implementation
    Route::post('gallery/category/{category_id}/update', 'GalleryController@update')->name('updatePhotoCategory');//make frontend and implementation
    Route::get('gallery/photo/{id}/delete', 'GalleryController@deletePhoto')->name('deletePhoto');                  //make frontend and implementation
    Route::get('gallery/category/{category_id}/confirm-deleting', 'GalleryController@confirmDeleting')->name('confirmDeletingPhotoCategory');                         //make frontend and implementation
    Route::get('gallery/category/{category_id}/delete', 'GalleryController@deleteCategory')->name('deletePhotoCategory');//make frontend and implementation

    Route::get('procedures', 'ProceduresController@index')->name('proceduresList');
    Route::get('procedure/create', 'ProceduresController@create')->name('procedureCreate');
    Route::post('procedure/store', 'ProceduresController@store')->name('procedureStore');
    Route::get('procedure/{slug}', 'ProceduresController@edit')->name('showProcedureAdmin'); // url, controller
    Route::post('procedure/{slug}/update', 'ProceduresController@update')->name('procedureUpdate');
    Route::get('procedure/{slug}/delete', 'ProceduresController@destroy')->name('procedureDelete');
    Route::get('procedure/{slug}/confirm-deleting', 'ProceduresController@confirmDeleting')->name('procedureConfirmDeleting');

//    Route::get('demo', function (){
//        return view('demo');
//    });
    Route::get('procedure/{slug}/hide', 'ProceduresController@hide')->name('procedureHide');
    Route::get('procedure/{slug}/activate', 'ProceduresController@activate')->name('procedureActivate');

});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
